# Firmwares for MicroPython on [Pycom boards](https://docs.pycom.io/products/#development-boards)

Look at the [Pycom Firmware Updater tool](https://docs.pycom.io/updatefirmware/device/) and [offline Pycom Firmwares site](https://docs.pycom.io/advance/downgrade/) for official Pycom firmwares for the Pycom xxPy boards. But current firmwares are provided by Pycom only with Pybytes, single precision float point numbers (SP/FP32) and threads.

All Pycom MicroPython firmwares here were compiled from the [Pycom MicroPython source code](https://github.com/pycom/pycom-micropython-sigfox), are not official and don't have any warranty, but all steps to build are open and can be reproduced.

## Firmware features

The Pycom MicroPython firmwares here are named in the form :  
```<xxPy board name>_<optional 'pybytes_'><optional 'ulab_'><'sp' or 'dp'>_<thread>_<version>_<date>.tar.gz``` 
where :

- [xxPy board](https://docs.pycom.io/products/#development-boards) name can be :
  - `WiPy`, for the [WiPy 2 board](https://docs.pycom.io/datasheets/development/wipy2/) (ESP32 with 520 kB of internal RAM and 4 MB of QSPI flash) and [WiPy 3 board](https://docs.pycom.io/datasheets/development/wipy3/) (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash);
  - `LoPy`, for the [LoPy v1 board](https://docs.pycom.io/datasheets/development/lopy/) (ESP32 with 520 kB of internal RAM  and 4 MB of QSPI flash, plus LoRa);
  -  `LoPy4`, for the [LoPy4 board](https://docs.pycom.io/datasheets/development/lopy4/)  (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash, plus LoRa and Sigfox);
  - `SiPy`, for the [SiPy board](https://docs.pycom.io/datasheets/development/sipy/) (ESP32 with 520 kB of internal RAM  and 4 MB of QSPI flash, plus Sigfox);
  - `GPy`, for the [GPy board](https://docs.pycom.io/datasheets/development/gpy/)  (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash, plus celular LTE CAT M1/NB1);
  - `FiPy`, for the [FiPy board](https://docs.pycom.io/datasheets/development/fipy/)  (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash, plus LoRa, Sigfox and celular LTE CAT M1/NB1).
- 'pybytes' means firmware built with [Pybytes](https://docs.pycom.io/pybytes/) support;
- 'ulab' means the [ulab native (in C) module](https://github.com/v923z/micropython-ulab), a NumPy-like array manipulation library, is included in the firmware;
- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.pycom.io/firmwareapi/micropython/_thread/) and allowing multithreading, and is always used by all Pycom MicroPython firmwares, here and the official ones.

For example : 
```FiPy_pybytes_ulab_dp_thread_v1.20.2.rc10-g87b47d16e_2020-08-06.tar.gz```  
means it is a v1.20.2.rc10-g87b47d16e firmware, from August 6 2020, built with Pybytes support, with ulab module included, double precision float point numbers and threads enabled, for FiPy board.


## Flashing firmware on ESP32

For flashing firmware on Pycom xxPy boards, see :
- [Pycom documentation "Updating Device Firmware"](https://docs.pycom.io/updatefirmware/device/).
