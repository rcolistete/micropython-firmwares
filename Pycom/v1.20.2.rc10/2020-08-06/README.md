# Pycom MicroPython [v1.20.2.rc10](https://github.com/pycom/pycom-micropython-sigfox/releases/tag/v1.20.2.rc10) firmwares for [Pycom boards](https://docs.pycom.io/products/#development-boards)

## Firmware features

The 24 Pycom MicroPython firmwares here are named in the form :  
```<xxPy board name>_<optional 'pybytes_'><'sp' or 'dp'>_<'thread'>_<version>_<date>.tar.gz``` 
where :

- [xxPy board](https://docs.pycom.io/products/#development-boards) name can be :
  - `WiPy`, for the [WiPy 2 board](https://docs.pycom.io/datasheets/development/wipy2/) (ESP32 with 520 kB of internal RAM and 4 MB of QSPI flash) and [WiPy 3 board](https://docs.pycom.io/datasheets/development/wipy3/) (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash);
  - `LoPy`, for the [LoPy v1 board](https://docs.pycom.io/datasheets/development/lopy/) (ESP32 with 520 kB of internal RAM  and 4 MB of QSPI flash, plus LoRa);
  - `LoPy4`, for the [LoPy4 board](https://docs.pycom.io/datasheets/development/lopy4/)  (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash, plus LoRa and Sigfox);
  - `SiPy`, for the [SiPy board](https://docs.pycom.io/datasheets/development/sipy/) (ESP32 with 520 kB of internal RAM  and 4 MB of QSPI flash, plus Sigfox);
  - `GPy`, for the [GPy board](https://docs.pycom.io/datasheets/development/gpy/)  (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash, plus celular LTE CAT M1/NB1);
  - `FiPy`, for the [FiPy board](https://docs.pycom.io/datasheets/development/fipy/)  (4 MB of external PSRAM/SPIRAM using QSPI bus and 8 MB of QSPI flash, plus LoRa, Sigfox and celular LTE CAT M1/NB1).
- 'pybytes' means firmware built with [Pybytes](https://docs.pycom.io/pybytes/) support;
- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.pycom.io/firmwareapi/micropython/_thread/) and allowing multithreading, and is always used by all Pycom MicroPython firmwares, here and the official ones.

For example : 
```LoPy4_pybytes_dp_thread_v1.20.2.rc10-gf0d60678c_2020-08-06.tar.gz```  
means it is a v1.20.2.rc10-gf0d60678c firmware, from August 6 2020, built with Pybytes support, double precision float point numbers and threads enabled, for LoPy4 board.


## (Optional) Building firmware with 'make' options

(TO DO) All Pycom MicroPython firmwares here were compiled using :