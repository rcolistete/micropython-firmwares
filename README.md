# MicroPython Firmwares

MicroPython firmwares built from source code with many options :

* [ulab native (in C) module](https://github.com/v923z/micropython-ulab), a NumPy-like array manipulation library, included in the firmware;
* single (SP/FP32) / double (DP/FP64) precision for float point numbers (except ESP8266, limited to FP32);
* threads (except ESP8266);
* Pybytes (for Pycom boards);
* network (for Pyboard v1.1/Lite v1.0);
* ESP-IDF 3.3 or 4.0 (for ESP32);
* etc;

for many MicroPython boards :

* Pyboard Lite v1.0 and Pyboard v1.1;
* Pyboard D SF2 / SF3 /  SF6;
* ESP8266;
* ESP32 (with or without PSRAM);
* Pycom boards FiPy / GPy / LoPy v1 / LoPy4 / SiPy / WiPy 2 / WiPy 3.

Current numbers :
* 67 firmware files without ulab with recent versions;
* 60 firmware files with ulab (v0.54.0 or v0.54.2) included.
