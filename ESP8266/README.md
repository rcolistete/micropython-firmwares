# Firmwares for MicroPython on [ESP8266](https://www.espressif.com/en/products/socs/esp8266/overview)

Look at the [MicroPython Download -> Firmware for Generic ESP8266 module site](https://micropython.org/download/esp8266/) for official firmwares for all types of ESP8266's.

All MicroPython firmwares here were compiled from the [MicroPython source code](https://github.com/micropython/micropython), are not official and don't have any warranty, but all steps to build are open and can be reproduced.


## Firmware features

The MicroPython firmwares here are named in the form :

```<esp8266>_<optional 'ulab_'><optional board memory>_<version>_date>.bin``` 

where :

- 'ulab' means the [ulab native (in C) module](https://github.com/v923z/micropython-ulab), a numpy-like array manipulation library, is included in the firmware;
- the board memory flash can be '1m' (1 MB), '512k' (512 kB) or >= 2 MB when not cited (default);
- sp/single precision/FP32 for float point numbers is always used, as MicroPython on ESP8266 doesn't support dp/double precision/FP64;
- there is no 'thread' support, i. e., no ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html), not allowing multithreading, as MicroPython on ESP8266 doesn't support it.

For example :  
```esp8266_ulab_1m_v1.12-658-g3bab891e5_2020-07-26.bin```  
means it is a v1.12-658-g3bab891e5 firmware, from July 26 2020, with ulab module included, double precision float point numbers enabled, for ESP8266 boards with 1 MB of flash.


## Flashing firmware on ESP8266

For flashing '.bin' firmware on ESP8266, the 'esptool' software should be used, see the [MicroPython documentation for ESP8266, section 1.4. Deploying the firmware](https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html#deploying-the-firmware).